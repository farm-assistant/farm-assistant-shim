/*
 * Configuration is explicit by design. No magic names and guessing.
 */

const winston = require('winston');
const get = name => process.env[name];

const got = name => { // get or throw
  const value = get(name);
  if (value === undefined) {
    throw `${name} env variable is required x`;
  }
  return value;
}

module.exports = {
  got: got,
  latlon: got('FASS_LATITUDE') + "," + got('FASS_LONGITUDE'),
  password: got('X_PASSWORD'),
  http: {
    host: got('NX_HTTP_HOST'),
    port: got('NX_HTTP_PORT'),
  },
  gateway: {
    socketsPath: got('X_GATEWAY_SOCKETS_PATH'),
  },
  nodered: {
    httpRoot: got('X_NODERED_HTTP_ROOT'),
  },
  logger: new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({
        level: got('X_LOG_LEVEL'),
      }),
    ]
  })
};



