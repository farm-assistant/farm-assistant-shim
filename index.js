'use strict';
const RED 	     = require("../../red/red.js");
const events 	  = require("../../red/runtime/events");
const storage 	 = require("../../red/runtime/storage");
const fs 	      = require("fs");
const http 	    = require("http");
//const format 	= require('tz-format');
//const snmp      = require('snmpjs');


const request   = require('request');
const config    = require('./config');

const IPCSocketOut = 'socket out';
const first     = 5000; // start delay
const level     = require("level");
    
var nodeId2EntityId = function (id){
  return  parseInt(id.substr(0,id.indexOf('.')), 16)
};

var _NODES={}

var localFS = function(){
    return require (__dirname + "/localfs.js");
}

var clientRequest = function (req,method="POST"){
  return new http.ClientRequest({
    hostname: config.http.host,
    port: config.http.port,
    path: req.path,
    method: method,
    headers: {
      'x-ha-access': config.password,
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(req.body)
    }
  });
};

var getDB =  function() {
    return (RED.hasOwnProperty("_db") && RED._db) ? RED._db : null;
  };
  

module.exports = {
  nodeId2EntityId:nodeId2EntityId,
  clientRequest:clientRequest,
  localFS:localFS, 
  entityId2Node:function(eid){
    return _NODES.hasOwnProperty(eid) ? _NODES[eid] : null;
  },
  getDB:getDB,
};


    function restartFass(){
            console.log("restart fass");
            var jsonBody = JSON.stringify({id:-1});
            var request = clientRequest({
                    path: "/api/services/homeassistant/restart",
                    body: jsonBody
            });
            request.write(jsonBody);
            request.end();
    }

    function deleteFassNode(id,fapiEndpoint){
            console.log("delete-fass-node");
            var jsonBody = JSON.stringify({id:id});
            				
            var request = clientRequest({
                    path: fapiEndpoint,
                    body: jsonBody
            });
            request.write(jsonBody);
            request.end();
    }
    

    events.on("runtime-event",function(n) {
           console.log("RED Event", n);
          //console.log(events);
       });
    
    events.on("nodes:remove",function(n) {
           console.log("RED Removing ", n); 
       });
    
    events.on("nodes-started",function() {
        console.log("All nodes have startedx",RED._fassNodes);
        const dbPath = config.got("FASS_CONFIG_PATH") + "/nrcache.db";
        if (!RED._db)
            RED._db = level(dbPath);
        var checkDelete=false;
        var fassNodes = {};
        if (RED.hasOwnProperty("_fassNodes")){
             console.log("Cloning fassNodes");
             checkDelete=true;
             fassNodes = RED._fassNodes;
        }
      
        RED._fassNodes = {};
        RED.nodes.eachNode(function(n){
            var _node = RED.nodes.getNode(n.id);
            _NODES[nodeId2EntityId(n.id)] = _node;
            try{
               if (_node.hasOwnProperty("fapiEndpoint")){
            RED._fassNodes[nodeId2EntityId(n.id)] = _node.fapiEndpoint;
            console.log(_node.fapiEndpoint);
            }


        } catch (e) {
     ;
        //  console.error(e);
        }
      });
    
    if (checkDelete){
        var restart = false;
        Object.keys(fassNodes).forEach(
            function(element, key, _array) {
                if (!RED._fassNodes.hasOwnProperty(element)){
                    console.log("NOT Found ",fassNodes[element]);
                    var apiPath = fassNodes[element].replace("state","delete")
                    deleteFassNode(element,apiPath);
                    restart=true;
                }
            });
        if (restart)
            restartFass();
        }
});


function loadEvent(options){
  var results = "";
  try {
    console.log("Sending request")
    console.log(options)
    var req = http.request(options,function(res) {
      res.on('data', function (chunk) {
        console.log("***** Data");
        results = results + chunk;
        //TODO
      });
      res.on('end', function () {
        console.log("*****  Success");
        //TODO
      });
      res.on('error', function () {
        console.log("*****  Failure");
        //TODO
      });
    });
    req.write("");
    req.end("");
  } catch (e) {
    console.log(e);
  }
}




/*

const offset 	= 3600*1000;
const s= format(new Date((new Date().valueOf()-offset)),0) + ".000Z"
const e=format(new Date((new Date().valueOf())),0) + ".000Z"

var q = 'start=' + s +'&end=' + e;
q= q.replace(/\:\d\d\+00/g,"");
q= q.replace(/:/g,"%3A");

  var foo= new http.ClientRequest({
    hostname: "127.0.0.1",
    port: 7007,
    path: "/api/history/period/?"+ q ,
    method: "GET",
    headers: {
      'x-ha-access': config.got("X_PASSWORD"),
    }},function(res){

       
console.log('STATUS: ' + res.statusCode);
    console.log('HEADERS: ' + JSON.stringify(res.headers));

    res.setEncoding('utf8');
    res.on('data',function(chunk){
        console.log('BODY: ' + chunk);
    });
    
   });

   foo.end();

*/
